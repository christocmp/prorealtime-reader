package main

import (
	"bytes"
	"cloud.google.com/go/vision/apiv1"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/christocmp/screenshot"
	_ "github.com/mattn/go-sqlite3"
	"image"
	"image/png"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"
	//visionpb "google.golang.org/genproto/googleapis/cloud/vision/v1"
)

//config structs
type Config struct {
	ProScreenWindow ProscreenWindowConfig
	Api             ApiConfig
}

type ProscreenWindowConfig struct {
	Display  int    `json:"display"`
	Width    int    `json:"width"`
	Height   int    `json:"height"`
	Filename string `json:"filename"`
}

type ApiConfig struct {
	CloudVision CloudvisionApiConfig
}

type CloudvisionApiConfig struct {
	ApiKey      string `json:"api-key"`
	ApiKeyName  string `json:"api-key-name"`
	ApiFilePath string `json:"api-file-path"`
}

//db structs
type Stock struct {
	Id        int
	Name      string
	DateAdded string
	LastSeen  string
}

var config Config
var stockFile []string

func main() {
	loadConfig()

	//open db
	db, err := sql.Open("sqlite3", "./stocks.db")
	checkError(err)
	defer db.Close()

	// Capture each displays.
	n := screenshot.NumActiveDisplays()
	if n <= 0 {
		panic("Active display not found")
	}
	display := config.ProScreenWindow.Display
	bounds := screenshot.GetDisplayBounds(display) // binding of the main or defined display

	// a region we want to capture
	region := image.Rect(
		int(bounds.Min.X), int(bounds.Min.Y), //get bounds for current display
		bounds.Min.X+config.ProScreenWindow.Width, bounds.Min.Y+config.ProScreenWindow.Height) //get a square from offset coords

	img2, err := screenshot.CaptureRect(region)
	if err != nil {
		panic(err)
	}
	log.Println(config.ProScreenWindow.Filename)
	save(img2, config.ProScreenWindow.Filename)

	//Apply OCR to image
	processImageFile("./mock-data/cloud-vision/ocr-1.png") // file something like "./mock-data/cloud-vision/ocr-1.png"

	//OCR results applied to stock list structure
	log.Println("prorealtime OCR image data: ")
	log.Println(stockFile)

	//Looking to remove what is not in file and signal this fact
	//checkForDropouts(*db)

	//Looking to add new entrants to our file and signal this fact
	checkForEntrants(*db)
}

func checkForEntrants(db sql.DB) [2]string { //oops did the other one loop through file and add any missing ones
	var found_id int64
	var errorMessage [2]string
	datetime := time.Now()

	log.Println("Started checking for dropouts")
	for _, stock := range stockFile {
		if stock == "" {
			continue
		}

		log.Println("checking db against" + stock)

		row := db.QueryRow("SELECT id FROM stocks WHERE name = ?", stock).Scan(&found_id)

		switch {
		case row == sql.ErrNoRows:
			{
				log.Println("No record found adding: " + stock)

				stmt, err := db.Prepare("INSERT INTO stocks (name, date_added, last_seen) VALUES (?,?,?);")

				rows, err := stmt.Exec(
					stock,
					datetime, //signal.SignalDatetime,
					datetime,
				)

				if err != nil {
					log.Fatal(err)
				} else {
					count, err2 := rows.RowsAffected()
					if err != nil {
						log.Fatal(err2)
					} else {
						errorMessage[0] = "200"
						errorMessage[1] = fmt.Sprintf("rows inserted: %d\n", count)
						log.Printf("%s - %s", errorMessage[0], errorMessage[1])
					}
				}

			}
		case row != nil && row != sql.ErrNoRows:
			{
				errorMessage[0] = "500"
				errorMessage[1] = "Unable to connect to db"
				log.Printf("%s - %s", errorMessage[0], errorMessage[1])
				return errorMessage
			}
		default:
			{
				//stock already exists update last seen
				log.Println("Record present updating last seen: " + stock)
				statement, err := db.Prepare("UPDATE stocks SET last_seen = ? WHERE id = ?;")

				checkError(err)

				rows2, err := statement.Exec(
					datetime,
					found_id, //signal.SignalDatetime,
				)

				if err != nil {
					log.Fatal(err)
				} else {
					count, err2 := rows2.RowsAffected()
					if err != nil {
						log.Fatal(err2)
					} else {
						errorMessage[0] = "200"
						errorMessage[1] = fmt.Sprintf("rows updated: %d\n", count)
						log.Printf("%s - %s", errorMessage[0], errorMessage[1])
					}
				}

			}
		}
	}
	log.Println("Finished checking for dropouts")

	errorMessage[0] = "200"
	errorMessage[1] = "OK"
	return errorMessage

}

func getAllRows(db sql.DB) {
	var stock Stock //db struct representing row
	rows, err := db.Query("select id, name, date_added, last_seen from stocks")
	defer rows.Close()

	checkError(err)
	for rows.Next() {
		err := rows.Scan(&stock.Id, &stock.Name, &stock.DateAdded, &stock.LastSeen)
		checkError(err)
		fmt.Println(stock)
	}
}

func loadConfig() {
	configFile, err := os.Open("config.json")
	defer configFile.Close()

	if err != nil {
		log.Println(err)
		panic("Failed to load config.json")
	}
	log.Println("Reading config from config.json")

	byteValue, _ := ioutil.ReadAll(configFile)
	json.Unmarshal(byteValue, &config)

	//set the env var as per google api requirements

	os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", config.Api.CloudVision.ApiFilePath)
	/*
		dat, err := ioutil.ReadFile(os.Getenv("GOOGLE_APPLICATION_CREDENTIALS"))
		fmt.Print(string(dat))
	*/
}

// save *image.RGBA to filePath with PNG format.
func save(img *image.RGBA, filePath string) {
	file, err := os.Create(filePath)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	png.Encode(file, img)
}

func trimQuote(s string) string {
	if len(s) > 0 && s[0] == '"' {
		s = s[1:]
	}
	if len(s) > 0 && s[len(s)-1] == '"' {
		s = s[:len(s)-1]
	}
	return s
}

// detectDocumentText gets the full document text from the Vision API for an image at the given file path.
// see https://github.com/GoogleCloudPlatform/golang-samples/blob/master/vision/detect/detect.go

// detectText gets text from the Vision API for an image at the given file path.
func detectText(w io.Writer, file string) error {
	ctx := context.Background()
	client, err := vision.NewImageAnnotatorClient(ctx)
	if err != nil {
		return err
	}
	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()
	image, err := vision.NewImageFromReader(f)
	if err != nil {
		return err
	}

	//var client *vision.ImageAnnotatorClient // Boilerplate is inserted by gen.go
	annotations, err := client.DetectTexts(ctx, image, nil, 10)
	if err != nil {
		return err
	}

	if len(annotations) == 0 {
		fmt.Fprintln(w, "No text found.")
	} else {
		for _, annotation := range annotations {
			fmt.Fprintf(w, "%q\n", annotation.Description)
		}
	}
	return nil
}

func processImageFile(file string) {
	//cloud vision api call
	cloudVisionResponse := new(bytes.Buffer)
	detectText(cloudVisionResponse, file) //portion of the pro realtimewindow

	detectedText := cloudVisionResponse.String()

	ioutil.WriteFile("cloud-vision-response.txt", cloudVisionResponse.Bytes(), 0777)

	temp := strings.Split(detectedText, "\n")

	stocks := strings.Split(trimQuote(temp[0]), `\n`)

	//build up public var to compare with db
	for _, stock := range stocks {
		stockFile = append(stockFile, stock)
		//log.Println(stock)
	}
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
